/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import Drawer from 'material-ui/Drawer'
import {spacing, typography} from 'material-ui/styles'
import {white, blue600} from 'material-ui/styles/colors'
import MenuItem from 'material-ui/MenuItem'
import {Link} from 'react-router'

import Url from 'app/config/core/url'

const Style = StyleSheet.create({
 	logo: {
		cursor: 'pointer',
		fontSize: 22,
		color: typography.textFullWhite,
		lineHeight: `${spacing.desktopKeylineIncrement}px`,
		fontWeight: typography.fontWeightLight,
		backgroundColor: blue600,
		paddingLeft: 40,
		height: 56,
	},
	menuItem: {
		color: white,
		fontSize: 14
	}
});

class LeftDrawer extends React.Component {
	render() {
		let index = 1;
		const eachMenuItem = this.props.menus.map(function(menu) {
			return (
				<a key={index++} href={menu.link}>
					<MenuItem
						className={css(Style.menuItem)}
						primaryText={menu.text}
						leftIcon={menu.icon}
					/>
				</a>
			)
		})
		return (
			<Drawer
				docked={true}
				open={this.props.navDrawerOpen}>
					<div className={css(Style.logo)}>
						{this.props.name}
					</div>
					<div>
						{eachMenuItem}
					</div>
			</Drawer>
		);
	}
}

export default LeftDrawer;
