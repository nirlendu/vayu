/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'
import moment from 'moment'

import Paper from 'material-ui/Paper'
import Subheader from 'material-ui/Subheader'
import {typography} from 'material-ui/styles'
import { grey400 } from 'material-ui/styles/colors'

import GlobalStyles from '../../styles'

const Style = StyleSheet.create({
	Paper: {
		minHeight: 344
	},
	Subheader: {
		fontSize: 24,
		fontWeight: [typography.fontWeightLight],
		backgroundColor: [grey400],
		color: 'white'
	},
	PieChartDiv: {
		height: 290,
		width: '100%',
		textAlign: 'center'
	},
	CampaignDetailHeader: {
		fontSize: 18,
		color: 'black'
	},
	CampaignDetail: {
		fontSize: 15,
		fontWeight: [typography.fontWeightLight],
		color: 'grey'
	},
	CampaignDetailBox: {
		padding: '2% 10%'
	}
});

class CurrentCampaigns extends React.Component {

	render() {
		let index = 1;
		let validFrom = moment.unix(this.props.data.validFrom/1000).format("DD/MM/YYYY");
		let validTill = this.props.data.validTill == null ? 'Not Provided' : moment.unix(this.props.data.validTill).format("DD/MM/YYYY");
		return (
			<Paper className={css(Style.Paper)}>
				<Subheader className={css(Style.Subheader)}>Current Campaigns</Subheader>
				<div style={GlobalStyles.clear}/>
				<div className="row">
					<div className={css(Style.PieChartDiv)}>
						<div className={css(Style.CampaignDetailBox)}>
							<div className={css(Style.CampaignDetailHeader)}>
								Campaign ID
							</div>
							<div className={css(Style.CampaignDetail)}>
								{this.props.data.campaignId}
							</div>
						</div>
						<div className={css(Style.CampaignDetailBox)}>
							<div className={css(Style.CampaignDetailHeader)}>
								Details
							</div>
							<div className={css(Style.CampaignDetail)}>
								{this.props.data.campaignDetails}
							</div>
						</div>
						<div className={css(Style.CampaignDetailBox)}>
							<div className={css(Style.CampaignDetailHeader)}>
								Valid From
							</div>
							<div className={css(Style.CampaignDetail)}>
								{validFrom}
							</div>
						</div>
						<div className={css(Style.CampaignDetailBox)}>
							<div className={css(Style.CampaignDetailHeader)}>
								Valid Till
							</div>
							<div className={css(Style.CampaignDetail)}>
								{validTill}
							</div>
						</div>
					</div>
				</div>
			</Paper>
		)
	}
}

export default CurrentCampaigns;
