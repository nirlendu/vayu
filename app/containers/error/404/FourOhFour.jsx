/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from  'app/config/core/style'

const Style = StyleSheet.create({
	Wrapper : {
		[CoreStyle.PC.BREAKPOINT]:{
			padding: '5% 0px',
			fontSize: '25px',
			color: [CoreStyle.COLOR.BLUE],
			textAlign: 'center',
			fontWeight: 'bold',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			padding: '5% 0px',
			fontSize: '25px',
			color: [CoreStyle.COLOR.BLUE],
			textAlign: 'center',
			fontWeight: 'bold',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			padding: '5% 0px',
			fontSize: '25px',
			color: [CoreStyle.COLOR.BLUE],
			textAlign: 'center',
			fontWeight: 'bold',
		},
	},
	Link:{
		color: [CoreStyle.COLOR.RED],
	}
})

export default class FourOhFour extends React.Component {
	render() {
		return (
			<div className={css(Style.Wrapper)}>
				OOPS! The page you're trying to access does not exist<br/>
				<br/><br/>
				In the meanwhile, <span><a className={css(Style.Link)} href="/">let us surprise you</a></span><br/>
			</div>
		)
	}
}
