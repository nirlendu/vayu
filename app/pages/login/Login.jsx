/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import request from 'superagent'

import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form'

import Form from 'app/containers/login/Form'

import 'app/assets/css/redux-form.css'

const reducer = combineReducers({
  form: reduxFormReducer
})

const store = createStore(reducer)

class Login extends React.Component {

	onSubmit(value){
		request
			.post('/login')
			.send(value)
			.set('Accept', 'application/json')
			.end(function(error, response){
				if (error || !response.ok) {
					//TODO
				}
				window.location.replace('/');
			}
		);
	}

	render() {
		return (
			<Provider store={store}>
				<div>
					<Form
						onSubmit={this.onSubmit}
					/>
				</div>
			</Provider>
		)
	}
}

export default Login
