/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import Helmet from 'react-helmet'

import Url from 'app/config/core/url'
import PanallyMetaTags from 'app/utils/meta-generator'

export default class Include extends React.Component {
	
	render() {
		const CoreTitle = 'Dashboard Login - Panally';
		const CoreDescription = 'Panally Dashboard helps you manage business efficiently';
		const CoreImage = Url.Static.App.Endpoint + 'favicon/ms-icon-310x310.png';
		const CoreUrl = '/';

		const Description = CoreDescription;
		const OgTitle = CoreTitle;
		const OgImage = CoreImage;
		const OgDescription = CoreDescription;
		const OgUrl = CoreUrl;
		const TwitterCard = CoreTitle;
		const TwitterTitle = CoreTitle;
		const TwitterDescription = CoreDescription;
		const TwitterImageSrc = CoreImage;

		const MetaTags = PanallyMetaTags (
			Description, 
			OgTitle,
			OgImage,
			OgDescription,
			OgUrl,
			TwitterCard,
			TwitterTitle,
			TwitterDescription,
			TwitterImageSrc
		);

		return (
			<Helmet
				htmlAttributes={{lang: "en", amp: undefined}} // amp takes no value
				title="Panally"
				titleAttributes={{itemprop: "name", lang: "en"}}
				meta={MetaTags}
				link={[
					{rel:"apple-touch-icon", sizes:"180x180", href: Url.Static.App.Endpoint + "favicon/apple-icon-180x180.png"},
					{rel:"icon", type:"image/png", sizes:"192x192",href: Url.Static.App.Endpoint + "favicon/android-icon-192x192.png"},
					{rel:"icon", sizes:"16x16", type:"image/png", href: Url.Static.App.Endpoint + "favicon/favicon-16x16.png"},
					{rel:"icon", sizes:"32x32", type:"image/png", href: Url.Static.App.Endpoint + "favicon/favicon-32x32.png"},
					{rel:"manifest", href:"/manifest.json"},
					{rel:"stylesheet", type:"text/css", href:"https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"}
				]}
				script={[
					{type: "text/javascript", innerHTML: `				
					`}
				]}
				style={[
				  {
					type: "text/css", cssText: `
					.m-b-15 {
					  margin-bottom: 15px
					}
					/* General Universal Settings */
					body{
						margin: 0px;
						height: 100%;
						font-family: 'Roboto', sans-serif !important;
						font-weight: 200;
						line-height: 1.4;
						background-color: #f1f1f1;
					}
					body, html, #app, #app > div{
					  height: 100%;
					}
					/* Waiting Placeholder */
					@keyframes placeHolderShimmer{
						0%{
							background-position: -468px 0
						}
						100%{
							background-position: 468px 0
						}
					}
					`
				}
				]}
			/>
		);
	}
}
