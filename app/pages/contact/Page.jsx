/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from  'app/config/core/style'

import Include from 'app/pages/contact/Include'

import ContactBody from'app/containers/contact/ContactBody'

const Style = StyleSheet.create({
	Wrapper : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '85%',
			margin: '5% auto',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			paddingBottom: '15%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			paddingBottom: '15%',
		},
	},
})

export default class Page extends React.Component {
	render() {
		return (
			<div id="react-mount">
				<Include/>
				<div className={css(Style.Wrapper)}>
					<ContactBody/>
				</div>
			</div>
		)
	}
}
