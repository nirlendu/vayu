/**
 * Copyright - Panally Internet
 */

var Enum = require('enum');

var Keys = {
	'Msg91': '169793A6FmgU4Nv5990b708',
	'GoogleAnalytics': 'UA-92611381-1',
	'SmoochIo': '5vhcjmy5t57nshmwi3r262fwq'
}

module.exports = new Enum(Keys).toJSON();