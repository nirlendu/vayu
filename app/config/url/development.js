/**
 * Copyright - Panally Internet
 */

const Enum = require('enum');

const ConstUrlsDev = {
	'CORS' : '',
	'MessageUrl': 'http://api.msg91.com/api/v2/sendsms',
	'OtpUrl': 'http://api.msg91.com/api/sendotp.php',
	'Auth': 'http://localhost:3000/api/auth/',
	'Api' : 'http://localhost:9000/v1/',
	'Static': {
		'Host': 'http://localhost:3000',
		'Font': '/assets/fonts/',
		'Media': '/assets/img/',
		'Css': '/assets/css/',
		'Js': '/assets/js/',
		'App': {
			'Endpoint': '/assets/img/app/'
		}
	},
	'Fetch': {
		'Index': 'http://localhost:3000/api/index/',
		'Business': 'http://localhost:3000/api/business/'
	},
	'Post': {
		'Register': 'http://localhost:3000/api/register',
		'ValidateRef': 'http://localhost:3000/api/validate-ref',
		'Referral': 'http://localhost:3000/api/referral',
		'ValidateRedeem': 'http://localhost:3000/api/validate-redeem',
		'Redeem': 'http://localhost:3000/api/redeem'
	}
}

module.exports = new Enum(ConstUrlsDev).toJSON();