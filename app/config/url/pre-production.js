/**
 * Copyright - Panally Internet
 */

const Enum = require('enum');

const ConstUrlsPreProduction = {
	'CORS' : '',
	'MessageUrl': 'https://control.msg91.com/api/postsms.php',
	'OtpUrl': 'http://api.msg91.com/api/sendotp.php',
	'Auth': 'https://prod-business.panally.com/api/auth/',
	'Api' : 'https://prod-api.panally.com/v1/',
	'Static': {
		'Host': 'https://prod-business-assets.panally.com',
		'Font': 'https://prod-business-assets.panally.com/static/fonts/',
		'Media': 'https://prod-business-assets.panally.com/static/img/',
		'Css': 'https://prod-business-assets.panally.com/static/css/',
		'Js': 'https://prod-business-assets.panally.com/static/js/',
		'App': {
			'Endpoint': 'https://prod-business-assets.panally.com/static/img/app/'
		},
	},
	'Fetch': {
		'Index': 'https://prod-business.panally.com/api/index',
		'Business': 'https://prod-business.panally.com/api/business/'
	},
	'Post': {
		'Register': 'https://prod-business.panally.com/api/register',
		'OtpRef': 'https://prod-business.panally.com/api/otp-ref',
		'Validate': 'https://prod-business.panally.com/api/validate',
		'Redeem': 'https://prod-business.panally.com/api/redeem',
		'Redeem': 'https://prod-business.panally.com/api/redeem'
	}
}

module.exports = new Enum(ConstUrlsPreProduction).toJSON();