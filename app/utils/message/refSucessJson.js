/**
 * Copyright - Panally Internet
 */

module.exports = function(messageData){
	
	let generatedJson = {};
	generatedJson.sender = messageData.businessMsgId;
	generatedJson.route = '4';
	generatedJson.country = '91';
	generatedJson.sms = [];
	
	const link = 'panally.com/r/' + messageData.campaignId;

	let firstMessage = {};
	firstMessage.message = 'Your referral for ' + messageData.phoneNumber + ' at ' + messageData.businessName + ' is succesful! Get ' + messageData.campaignDetails + ' on your next visit. Refer more to get more. Click to share ' + link;
	firstMessage.to = [];
	
	firstMessage.to.push(messageData.refPhoneNumber);

	generatedJson.sms.push(firstMessage);

	return generatedJson;

}