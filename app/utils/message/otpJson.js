/**
 * Copyright - Panally Internet
 */

const Keys = require('app/config/core/keys');

module.exports = function(messageData){
	
	let generatedJson = {};
	generatedJson.authkey = Keys.Msg91;
	generatedJson.sender = 'PANLLY';
	generatedJson.mobile = '91' + messageData.phoneNumber;
	generatedJson.message = 'Your OTP is : ' + messageData.password;
	generatedJson.otp = messageData.password;
	generatedJson.otp_length = 6;

	return generatedJson;

}