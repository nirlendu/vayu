/**
 * Copyright - Panally Internet
 */

module.exports = function(messageData){
	
	let generatedJson = {};
	generatedJson.sender = messageData.businessMsgId;
	generatedJson.route = '4';
	generatedJson.country = '91';
	generatedJson.sms = [];
	
	const link = 'panally.com/r/' + messageData.campaignId;

	let firstMessage = {};
	firstMessage.message = 'Hi from ' + messageData.businessName + '. Refer friends and tell them to quote ur number on visit and both get ' + messageData.campaignDetails + '. Click to share ' + link;
	firstMessage.to = [];
	
	firstMessage.to.push(messageData.phoneNumber);

	generatedJson.sms.push(firstMessage);

	return generatedJson;

}