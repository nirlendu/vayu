/**
 * Copyright - Panally Internet
 */


/******************
 *
 *
 * Flow of the router config, () signifies next in the heirarchy
 * 
 * api-router --> static-router --> (main-router)
 *
 *
 *******************/


var React = require('react');
var ReactDOMServer = require('react-dom/server');
var ReactRouter = require('react-router');
const Helmet = require('react-helmet');
const StyleSheetServer = require('aphrodite').StyleSheetServer;
const fs = require('fs');

var match = ReactRouter.match;
var RouterContext = React.createFactory(ReactRouter.RouterContext);
var Provider = React.createFactory(require('react-redux').Provider);  

const Url = require('app/config/core/url');

require('babel-core/register')({
	presets: ['es2015', 'react']
});

const compressor = require('app/utils/es6-compressor');

// Importing the setup router
const router = require('app/router/api-router');

const AssetName = JSON.parse(fs.readFileSync('manifest.json'));

const commonJs = Url.Static.Js + AssetName['common.js'];

// The router settings file
const routes = require('app/config/routes.js').routes;

router.get('/oops/404', function(req, res) {
	match({routes: routes, location: req.url}, function(error, redirectLocation, renderProps) {
		if (error) {
			res.status(500).send(error.message)
		} else if (redirectLocation) {
			res.redirect(302, redirectLocation.pathname + redirectLocation.search)
		} else if (renderProps) {
			const head = Helmet.rewind();
			const pageJs = Url.Static.Js + AssetName['404.js'];
			const webPage = compressor`
				<!doctype html>
				<html>
					<head>
					</head>
					<body>
						<div id="react-mount"></div>
						<script src=${commonJs}></script>
						<script src=${pageJs}></script>
					</body>
				</html>
			`;
			res.header('Content-Type', 'text/html');
			res.write(webPage);
			res.end();
		} else {
			res.status(404).send('Not found')
		}
	});
});


router.get('/manifest.json', function(req, res) {
	const webPage = compressor`
		{
			"name": "Panally App",
			"short_name": "Panally",
			"start_url": "${Url.Static.Host}",
			"theme_color": "white",
			"background_color": "white",
			"display": "standalone",
			"icons": [
				{
					"src": "${Url.Static.App.Endpoint}favicon\/android-icon-36x36.png",
					"sizes": "36x36",
					"type": "image\/png",
					"density": "0.75"
				},
				{
					"src": "${Url.Static.App.Endpoint}favicon\/android-icon-48x48.png",
					"sizes": "48x48",
					"type": "image\/png",
					"density": "1.0"
				},
				{
					"src": "${Url.Static.App.Endpoint}favicon\/android-icon-72x72.png",
					"sizes": "72x72",
					"type": "image\/png",
					"density": "1.5"
				},
				{
					"src": "${Url.Static.App.Endpoint}favicon\/android-icon-96x96.png",
					"sizes": "96x96",
					"type": "image\/png",
					"density": "2.0"
				},
				{
					"src": "${Url.Static.App.Endpoint}favicon\/android-icon-144x144.png",
					"sizes": "144x144",
					"type": "image\/png",
					"density": "3.0"
				},
				{
					"src": "${Url.Static.App.Endpoint}favicon\/android-icon-192x192.png",
					"sizes": "192x192",
					"type": "image\/png",
					"density": "4.0"
				},
				{
					"src": "${Url.Static.App.Endpoint}favicon\/ms-icon-310x310.png",
					"sizes": "512x512",
					"type": "image\/png",
					"density": "4.0"
				}
			]
		}
	`;
	res.header('Content-Type', 'application/json');
	res.write(webPage);
	res.end();
});


router.get('/browserconfig.xml', function(req, res) {
	const webPage = compressor`
			<?xml version="1.0" encoding="utf-8"?>
<browserconfig><msapplication><tile><square70x70logo src="${Url.Static.App.Endpoint}favicon/ms-icon-70x70.png"/><square150x150logo src="${Url.Static.App.Endpoint}favicon/ms-icon-150x150.png"/><square310x310logo src="${Url.Static.App.Endpoint}favicon/ms-icon-310x310.png"/><TileColor>#ffffff</TileColor></tile></msapplication></browserconfig>
	`;
	res.header('Content-Type', 'application/xml');
	res.write(webPage);
	res.end();
});


router.get('/contact-form', function(req, res) {
	const webPage = compressor`
		<!doctype html>
		<html>
			<head>
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
				<title>Contact</title>
				<style type="text/css"> 
					html{ margin: 0; height: 100%; overflow: hidden; } 
					iframe{ position: absolute; left:0; right:0; bottom:0; top:0; border:0; } 
				</style>
			</head>
			<body>
				<iframe id="typeform-full" width="100%" height="100%" frameborder="0" src="https://panally.typeform.com/to/hIlxjQ"></iframe>
				<script type="text/javascript" src="https://s3-eu-west-1.amazonaws.com/share.typeform.com/embed.js"></script>
			</body>
		</html>
	`;
	res.write(webPage);
	res.end();
});

module.exports = router;